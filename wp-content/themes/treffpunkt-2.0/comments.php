<?php if(is_single()) : ?>
	<?php if ( have_comments() ) : ?>
		<h2>Kommentare</h2>
		<ol class="commentlist">
			<?php
				wp_list_comments( array('callback' => 'tf_comment') );
			?>
		</ol>
	<?php endif; ?>

	<?php comment_form( array('comment_notes_after' => '', 'title_reply' => 'Hinterlasse einen Kommentar', 'comment_notes_before' => '<p class="comment-notes">Deine E-Mail Adresse wird nicht veröffentlicht!</p>') ); ?>
<?php endif; ?>