<nav class="navbar navbar-default">
    <div class="container-fluid">
	<div class="row">
	    <div class="navbar-header col-md-2 col-md-offset-1">
		<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand  " href="#">TREFFPUNKT.</a>
	    </div>

	    <div class="navbar-collapse collapse" id="navbar">
	      <ul class="nav navbar-nav col-md-6">
		<!-- <?php wp_list_pages('title_li=&depth=2&sort_column=menu_order'); ?>-->
		  <!--Listet alle Seiten als Menü auf. Anpassen Menü greift nicht mehr!-->
		
		    <?php
		      $default = array (
			'theme_location' => 'primary',
			'container' => false,
			'menu_class' => 'nav navbar-nav',
			'depth' => 1,
			'walker' => new wp_bootstrap_navwalker()
		      );
		      wp_nav_menu($default); 
		    ?> 
	      </ul>
	      <form class="navbar-form navbar-left col-md-2 col-sm-1" role="search">
		<div class="form-group ">
		  <input type="text" class="form-control" placeholder="Suchen">
		</div>
		  <button type="submit" class="btn btn-default">Los</button>
	      </form>
	    </div>
	</div>
</div><!--/.nav-collapse -->
</nav>
</header><!-- /Header-->
