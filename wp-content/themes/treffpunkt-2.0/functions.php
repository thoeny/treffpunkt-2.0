<?php

/* Enable PHP sessions
***************************************************************************/
//if(!session_id()) { session_start(); }


/* Disable Session idle logout
***************************************************************************/
//remove_action( 'admin_enqueue_scripts', 'wp_auth_check_load' );


/* Thumbnails
***************************************************************************/
//add_theme_support( 'post-thumbnails', array('page') );
//add_image_size( 'slider', '940', '530', true );


/* Tinymce Editor Styles
***************************************************************************/
//add_editor_style('style_tinymce.css');


/* Languages
***************************************************************************/
//load_theme_textdomain('bin', get_template_directory().'/languages');


/* Navigation
***************************************************************************/
/* register navigation */
register_nav_menu('primary','Hauptnavigation');


/* Sidebar & Widgets
***************************************************************************/
// register sidebar widgets
// register_sidebar( array('name' => 'Widget', 'id' => 'widget-01', 'description' => 'Widget Area', 'before_widget' => '<li id="%1$s" class="widget-container %2$s">', 'after_widget' => '</li>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>') );


/* Admin Bar (hide)
***************************************************************************/
//add_filter('show_admin_bar', '__return_false');

/* Shortcode
***************************************************************************/
// Add Shortcode

function custom_shortcode() {
// Querry
$lastposts = array(
  'posts_per_page=4',
  'post_type'=> 'banner'
  );

$the_query = new WP_Query( $lastposts );
$return = "";

if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

  $return .= ' </div> <div class="banner col-md-12 text-center">
             <h2>'. get_the_title() .'</h2>
             <p>'. get_the_content() .'</p>
             <a href="kontakt" class="btn btn-info contact-button" role="button">'. get_field('banner_email').'</a>
           </div>
           <style type="text/css">
             .banner {
               background-image: url("'. get_template_directory_uri() .'/images/banner_hintergrund_1.JPG");
             }
           </style>
         ';


endwhile; else:

endif;
wp_reset_postdata();

  return $return;
}

add_shortcode( 'Banner', 'custom_shortcode' );

// function custom_shortcode() {
//
//   return '</div> <div class="banner col-md-12 text-center">
//             <h2>Nigg Seilbahnen GmbH</h3>
//             <p>«Transportieren Sie ihr Material effizient mit uns in die höhe»</p>
//             <a href="kontakt" class="btn btn-info contact-button" role="button">KONTAKTIEREN SIE UNS</a>
//           </div>
//
//         <style type="text/css">
//           .banner {
//           background-image: url("'. get_template_directory_uri() .'/images/banner_hintergrund_1.JPG");
//           }
//         </style>
//         ';
// }
// add_shortcode( 'Banner', 'custom_shortcode' );

/* Banner
***************************************************************************/
// Add anner Page

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'banner',
    array(
      'labels' => array(
        'name' => __( 'Banner' ),
        'singular_name' => __( 'Banner' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}


/* JavaScript
***************************************************************************/
add_action('wp_enqueue_scripts', 'bin_wp_enqueue_scripts');
function bin_wp_enqueue_scripts()
{
	wp_enqueue_script('jquery');

	wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', 'jquery');
	wp_enqueue_script('bootstrap');

	wp_register_script('functions', get_bloginfo('template_directory').'/functions.js', 'jquery');
	wp_enqueue_script('functions');
}

/* CSS
***************************************************************************/
add_action('wp_enqueue_scripts', 'bin_wp_enqueue_styles');
function bin_wp_enqueue_styles()
{
	wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap');

	wp_register_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
	wp_enqueue_style('fontawesome');

	wp_register_style('style', get_bloginfo('template_directory').'/style.css');
	wp_enqueue_style('style');
}


function load_fonts() {
            wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,800italic,800,700italic');
            wp_enqueue_style( 'googleFonts');
        }
    
    add_action('wp_print_styles', 'load_fonts');




/* Pagetitle for html <title>
***************************************************************************/
if( !function_exists('tf_blog_title') )
{
	function tf_blog_title()
	{
		global $page, $paged;
		wp_title( '|', true, 'right' );

		// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) { echo " | $site_description"; }

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 ) { echo ' | ' . sprintf( __( 'Page %s', 'bin' ), max( $paged, $page ) ); }
	}
}
/* Enable Postimages
***************************************************************************/
add_theme_support( 'post-thumbnails' );
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

/* Custom CSS for Adminbar
***************************************************************************/
/**
 * If the WordPress admin bar will be visible, enqueue our 'admin-bar-overrides' stylesheet
 * @uses is_admin_bar_showing()
 * @uses wp_enqueue_style()
 */
function grunwell_admin_bar_overrides() {
  if ( is_admin_bar_showing() ) {
    // I actually used wp_register_script() elsewhere and just ran `wp_enqueue_script( 'admin-bar-overrides' )`
    // here, but this will also do the trick
    wp_enqueue_style( 'admin-bar-overrides', get_bloginfo( 'template_url' ) . '/css/admin-bar.css', array( 'admin-bar' ), null, 'all' );
  }
}
add_action( 'init', 'grunwell_admin_bar_overrides' );



















/* Navwalker for Menu integration
***************************************************************************/

/**
 * Class Name: wp_bootstrap_navwalker
 * GitHub URI: https://github.com/twittem/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 3 navigation style in a custom theme using the WordPress built in menu manager.
 * Version: 2.0.4
 * Author: Edward McIntyre - @twittem
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */
class wp_bootstrap_navwalker extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		/**
		 * Dividers, Headers or Disabled
		 * =============================
		 * Determine whether the item is a Divider, Header, Disabled or regular
		 * menu item. To prevent errors we use the strcasecmp() function to so a
		 * comparison that is not case sensitive. The strcasecmp() function returns
		 * a 0 if the strings are equal.
		 */
		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
		} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		} else {
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			if ( $args->has_children )
				$class_names .= ' dropdown';
			if ( in_array( 'current-menu-item', $classes ) )
				$class_names .= ' active';
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$atts = array();
			$atts['title']  = ! empty( $item->title )	? $item->title	: '';
			$atts['target'] = ! empty( $item->target )	? $item->target	: '';
			$atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';
			// If item has_children add atts to a.
			if ( $args->has_children && $depth === 0 ) {
				$atts['href']   		= '#';
				$atts['data-toggle']	= 'dropdown';
				$atts['class']			= 'dropdown-toggle';
				$atts['aria-haspopup']	= 'true';
			} else {
				$atts['href'] = ! empty( $item->url ) ? $item->url : '';
			}
			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}
			$item_output = $args->before;
			/*
			 * Glyphicons
			 * ===========
			 * Since the the menu item is NOT a Divider or Header we check the see
			 * if there is a value in the attr_title property. If the attr_title
			 * property is NOT null we apply it as the class name for the glyphicon.
			 */
			if ( ! empty( $item->attr_title ) )
				$item_output .= '<a'. $attributes .'><span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
			else
				$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}
	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max
	 * depth and no ignore elements under that depth.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @see Walker::start_el()
	 * @since 2.5.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;
        $id_field = $this->db_fields['id'];
        // Display this element.
        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
	/**
	 * Menu Fallback
	 * =============
	 * If this function is assigned to the wp_nav_menu's fallback_cb variable
	 * and a manu has not been assigned to the theme location in the WordPress
	 * menu manager the function with display nothing to a non-logged in user,
	 * and will add a link to the WordPress menu manager if logged in as an admin.
	 *
	 * @param array $args passed from the wp_nav_menu function.
	 *
	 */
	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {
			extract( $args );
			$fb_output = null;
			if ( $container ) {
				$fb_output = '<' . $container;
				if ( $container_id )
					$fb_output .= ' id="' . $container_id . '"';
				if ( $container_class )
					$fb_output .= ' class="' . $container_class . '"';
				$fb_output .= '>';
			}
			$fb_output .= '<ul';
			if ( $menu_id )
				$fb_output .= ' id="' . $menu_id . '"';
			if ( $menu_class )
				$fb_output .= ' class="' . $menu_class . '"';
			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
			$fb_output .= '</ul>';
			if ( $container )
				$fb_output .= '</' . $container . '>';
			echo $fb_output;
		}
	}
}