<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php get_template_part('navigation'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php
				the_post();
				the_content();
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
