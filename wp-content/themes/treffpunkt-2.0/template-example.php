<?php
/**
 * Template Name: News Template
 */

/**
 * When a person browses to your website, WordPress selects which template to use for rendering that page.
 * As we learned earlier in the Template Hierarchy, WordPress looks for template files in the following order:
 *
 * 1 Page Template — If the page has a custom template assigned, WordPress looks for that file and, if found, uses it.
 * 2 page-{slug}.php — If no custom template has been assigned, WordPress looks for and uses a specialized template that contains the page’s slug.
 * 3 page-{id}.php — If a specialized template that includes the page’s slug is not found, WordPress looks for and uses a specialized template named with the page’s ID.
 * 4 page.php — If a specialized template that includes the page’s ID is not found, WordPress looks for and uses the theme’s default page template.
 * 5 index.php — If no specific page templates are assigned or found, WordPress defaults back to using the theme’s index file to render pages.
 */
 ?>
 <?php get_header(); ?>

<div class="container-fluid">
   <div class="wrapper">
     <div class="row">
   		<div class="col-md-5 col-md-offset-1">
   			<?php
   				the_post();
           the_content();
   			?>
   		</div>

 <div class="container-fluid">
 	<div class="row">
 		<div class="col-md-12 beitrag">
 			<?php
      // Querry
      $lastposts = array(
        'posts_per_page' => '4',
        'post_type'=> 'post',
        );

      $the_query = new WP_Query( $lastposts );

      if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

      $beitragsbild = get_field('beitragsbild');

        echo ' <div class="col-md-4 col-md-offset-1 post-image">
                   <img class="img-responsive post-image" data-toggle="lightbox" src="'.$beitragsbild.'">
               </div>
               <div class="col-md-5">
                 <h4>'. get_the_title() .'</h4>
                 <p>'. get_the_content() .'</p>
               </div>
               ';


      endwhile; else:

      endif;
      wp_reset_postdata();

     return $return;
 			?>
 		</div>
 	</div>
 </div>
 </div>
 </div>
 </div>


 <?php get_footer(); ?>
