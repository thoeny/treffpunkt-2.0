<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();


<?php

/* header */
get_template_part('views/navigation/main');

/* content */
echo '<div class="news-wrapper content-wrapper">';
	echo '<div class="container">';

		echo '<div class="row">';
			echo '<div class="col-md-12">';
				echo '<h1>Suchresultate</h1>';
			echo '</div>';
		echo '</div>';
	
		echo '<div class="row">';
			echo '<div class="col-md-8 col-content">';
				if (have_posts())
				{
					while (have_posts())
					{
						the_post();

						// Suchbegriffe
						$keys= explode(" ",$s);

						// Titel
						$title = preg_replace('/('.implode('|', $keys) .')/iu', '<span style="background-color: yellow;">\0</span>', get_the_title() );

						// damit der gesamte text eingelesen wird
						global $more;
						$more = 1;

						// Content nach suchbegriffen durchsuchen
						$content = tf_highlightSearch(strip_tags(get_the_content()), $s, 150);
						$content = preg_replace('/('.implode('|', $keys) .')/iu', '<span style="background-color: yellow;">\0</span>', $content[0] );

						echo '<div class="blog-post">';
							echo '<h2><a href="'.get_permalink().'">'.$title.'</a></h2>';
							//echo '<p>'.get_the_time('j. F Y').'</p>';
							echo '<p>'.$content.'</p>';

							echo '<ul class="content-social">';
								echo '<li><a href="'.get_permalink().'" class="btn btn-white">Weiter</a></li>';
							echo '</ul>';
						echo '</div>';
					}
				
					wp_reset_query();
				}
				else
				{
					echo '<div class="blog-post">';
						echo '<p>'.sprintf(__('Die Suche nach <strong>%1$s</strong> liefert keine Treffer.'), get_search_query() ).'</p>';
					echo '</div>';
				}
			echo '</div>';
			echo '<div class="col-md-4 col-sidebar">';
			
				// next and prev
				echo '<div class="post-navigation">';
					if(get_previous_posts_link()) { echo '<span class="post-prev">'.get_previous_posts_link('previous').'</span>'; }
					if(get_next_posts_link()) { echo '<span class="post-next">'.get_next_posts_link('next').'</span>'; }
				echo '</div>';
				
			echo '</div>';
		echo '</div>';

	echo '</div>';
echo '</div>';

get_footer();