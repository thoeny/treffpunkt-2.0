<?php get_header(); ?>

<div class="container-fluid">
  <div class="wrapper">
    <div class="row" style="text-align: center;">
  		<?php
        the_post();
        the_content();
      ?>
        </div>

  </div>
</div>

<?php get_footer(); ?>
